import datetime

def create_period_list(period_range):
    """
    @params:
    - period_range: tuple or list of start and end date (start, end),
      formatted as (yyyy-mm-dd, yyyy-mm-dd)
    
    @returns:
    - list of all date pair increments from the start to the end date as tuples,
    [ (start, start+1), ... , (end-1, end) ]

    @example:
    - period_range = ('2020-01-01', '2020-06-30')

    - returns : [ ('2020-01-01', '2020-01-02'), ... , ('2020-06-29', '2020-06-30') ]
    """
    
    add_a_day = datetime.timedelta(days=1)
    current = period_range[0]
    end = period_range[1] 

    all_date_pairs = []
    while current != end: 
        date_components = [int(i) for i in current.split('-')]
        current_date_object = datetime.date(year=date_components[0], month=date_components[1], day=date_components[2])
        next_day = (current_date_object + add_a_day).strftime("%Y-%m-%d") 
        all_date_pairs.append((current, next_day))
        current = next_day 

    return all_date_pairs
        
