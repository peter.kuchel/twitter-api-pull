
class importObjects(object):

    """
    ########## FULL DESCRIPTION AND RUN THROUGH: ##########

    Things are explained in great detail incase you are NOT familiar with python, but if 
    there is anything you are confused about feel free to reach out

    (please note, method = function = stuff with 'def')

    you will need to install the following libraries (present at the top of the scripts):
        - openpyxl
        - searchtweets


    run down of the scripts, more details explained in them: 
    reference_objects : 
        - contains all the inputs you want to use (files paths, hashtags, query requirements, etc)

    collect_twitter_data : 
        - has the main method make the api calls, call full_collection to have the api make calls for 
          all you hashtags and periods below in the TO_FIND dictionary, saves all the twitter objects too
        - contains a class which implements the api wrapper
        - also has a method to test the api (makes one call)

    periods : 
        - contains a method to create the date ranges
    
    collect_object_info : 
        - if you need more than just the tweets and their data, all the objects will (if you do it correctly) be saved,
          and you can open them up and get the data you need from them. Might require some additional coding
    
    excel_export :
        - method used to export things to excel files, please note, in the example files in the gitlab, you need to have
          the 'template' sheets in those present in the excel document you use, or else your results will not
          be put into excel
    
    Below are the inputs you should set to get the tweets you need, i recommend exploring the code and trying to
    see where these come in at. 
    """

    ###################################################################################################################

    # dictionary of all hashtags and the periods to search, use this to input your requirements 
    TO_FIND = to_find = {
                    "#buildthewall": ["2017-07-01","2017-09-01"],
                    "#rejectcaa_nrc": ["2019-12-01","2020-02-01"],
                    "#supportcaaandnrc": ["2019-12-01","2020-02-01"],
                    "#abolishice": ["2018-06-15","2018-08-15"] 
                        }

    ### FULL SEARCH PARAMS ###

    """
    For all the params below, make sure they are in the same directory (use a folder to store all the raw tweets
    and tweet objects unless you want 200+ files in your main folder lmao)
    """
    # THE YAML FILE YOU ARE USING TO COLLECT YOUR TWEETS #
    # be sure to specific the account type you have, the endpoint you want, and your bearer_token
    # (please note depending on the account type you have, the api might want you to use different c
    #  credentials e.g. api key and secret key)
    CRED_FILE = "full_archive_creds.yaml"
    # what to locate in the yaml file, feel free to change it 
    YAML_KEY = "search_tweets_fullarchive"

    # how many tweets do you want returned per call to the api (recommend doing 500 since that is the maximum)
    RESULTS_PER_CALL_FULL = 500

    # what you want in the query of your tweets 
    # this example reads out as: not a retweet and not a reply 
    QUERY_REQUIRMENTS = " -is:retweet -is:reply"

    # path to folder you want to dump all the twitter objects (raw_tweets is the name of the folder)
    # example of mine 

    RAW_DUMP_PATH = "C:\\Users\\peter\\Desktop\\python\\Twitter\\raw_tweets\\"

    # EXCEL FILE, PLEASE USE THE TEMPLATE SHEET PROVIDED, OR IT WILL NOT WORK (LMAO BRUH I KNOW HAHA) #
    FULL_SEARCH_FILE = "tweetOutFile.xlsx"

    # IF YOU WANT CUSTOM RESULTS, USE THIS FILE AS AN EXAMPLE (INCLUDES THE TEMPLATE SHEET)
    GET_RESULTS_FILE = "fullTweets.xlsx"
