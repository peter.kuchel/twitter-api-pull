from openpyxl import load_workbook  

def excel_dump(all_tweets, hashtag, excel_name):

    wb = load_workbook(excel_name)
    current_sheets = wb.sheetnames 
    if hashtag in current_sheets:
        over_write = wb[hashtag]
        wb.remove_sheet(over_write)
        
    template_sheet = wb['Template']
    new_sheet = wb.copy_worksheet(template_sheet)
    new_sheet.title = hashtag

    ws = wb[hashtag]
    write_spot = ws.max_row + 1 #plus one because of headers 
    
    for tweet in all_tweets:
        for i, t in enumerate(tweet):
            ws.cell(row= write_spot, column= i+1).value = t
        write_spot+= 1
    
    wb.save(excel_name)
    wb.close()

