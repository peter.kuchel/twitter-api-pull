import pickle
from reference_objects import importObjects 
from periods import create_period_list 
from excel_export import excel_dump 
import requests
from searchtweets import load_credentials, gen_rule_payload, collect_results 
from time import sleep 

FILE_PATH = importObjects.RAW_DUMP_PATH 

class pickle_support(object):
    """
    just a class to support pickling objects
    """

    def dump_data(self, file_location, data):
        outfile = open(file_location, 'wb')    
        pickle.dump(data, outfile)
        outfile.close()
    
    def load_data(self, file_location):
        with open(file_location, 'rb') as infile:
            return pickle.load(infile)

class twitter_api(object):

    def __init__(self, file_name, yaml_key):
        """
        api handler for the twitter api, uses the searchtweets wrapper library (made by twitter developers)

        @params:
        - file_name: name of the yaml file with credentials (account_type, endpoint, bearer_token)
        - yaml_key: where are the creds located under in the yaml file
        """
        self.prem_search_args = load_credentials(filename=file_name, yaml_key=yaml_key, env_overwrite=False)

        # #Just leaving this here as example of what might be added to a query
        # self.prem_query = " -is:retweet -is:reply" 
        

        print(self.prem_search_args)
    
    def check_token(self):
        test_endpoint = "https://api.twitter.com/1.1/application/rate_limit_status.json"
        token = self.prem_search_args['bearer_token']

        get_request = requests.get(test_endpoint, params=token)
        print(f"Bearer token status code is: {get_request.status_code}")

    
    def make_query(self, hashtag, query_requirements, dates, results_to_get):

        ### making the query for the hashtag and the specifications ###
        query = hashtag + query_requirements

        print(f"from: {dates[0]}, to: {dates[1]} for {hashtag}")

        ### generating the rules for the query ###
        rules = gen_rule_payload(pt_rule=query,
                                 results_per_call= results_to_get, 
                                 from_date= dates[0],
                                 to_date= dates[1])

        print(rules)
        tweets = collect_results(rule=rules, max_results=results_to_get, result_stream_args=self.prem_search_args)
        return tweets 

def full_collection(hashtags, to_find, api, results_per_call, query_reqs, excel_file):
    """
    Will make requests to the Twitter API for the hashtags listed
    @params:
        - hashtags: hashtags you want to search for
        - to_find: main object with date ranges (see in reference objects)
        - api: twitter api object 
        - results_per_call: how many tweets to return (max is 500 for premium)
        - query_reqs: additional requirements for your query e.g.; ( -is:retweet -is:reply ) 
          (e.g. -is:retweet means :: is not a retweet)
        - excel_file: file to output tweets to (will only return dates and tweets)

    The raw tweet objects will be saved in the path you specify in reference_objects (see example)
    If you do not want just the text and date, all the information will be in the twitter objects 
    which are saved as pickle files, and you can freely gather what you need from each object. 
    
    Feel free to comment out or change the code with the excel file if you do not need it and just want the raw tweets

    Use the collect_object_info.py to do so, it has an example and some old code you can use 
    """
    print("FULL COLLECTION")
    sleep(3) 

    ### iterating through all hashtags listed ### 
    for tag in hashtags:
        print(f"---{tag}---")

        ### pickle object to save results just in case ###
        pickle_object = pickle_support()

        ### collecting the date ranges for the different tags ###
        date_range = to_find[tag]
        full_date_pairs = create_period_list(date_range)

        ### loop to collect all desired tweets ### 
        all_tweets = []
        for pair in full_date_pairs:
            # print(f"--{pair}--")

            ### create a file path for storing the raw data ###
            file_loc = FILE_PATH + f"{tag}_{pair}_tweet_object"

            ### make request to the api 
            tweets = api.make_query(hashtag= tag, 
                                    results_to_get= results_per_call, 
                                    dates= pair, 
                                    query_requirements= query_reqs)

            ### save the raw data, so no matter what it can be accessed ### 
            pickle_object.dump_data(file_loc, tweets)

            ### collect all the tweets and dates they were created into tuple then append into main list ### 
            current_pair_all = [(t['created_at'],' '.join(t.text.split())) for t in tweets]
            print(f"\t-- Tweets for that day: {len(current_pair_all)} --")
            [all_tweets.append(i) for i in current_pair_all]

            ### maximum number of requests that can be made per minute is 60, this gives ample time between ###
            sleep(5) 

        # print(all_tweets)

        ### check to see whether none where returned ### 
        if all_tweets == []:
            print(f"{tag} has no tweets in range: {full_date_pairs[0]}, {full_date_pairs[-1]}")

        ### dump all the tweets gathered for that hashtag into the excel file ### 
        excel_dump(all_tweets, tag, excel_file)
        sleep(1)

def test_run(hashtags, to_find, api, results_per_call, query_reqs, excel_file):
    """
    THIS IS TO MAKE A TEST TO THE API, IT WILL MAKE 1 CALL ONLY

    IT HAS THE SAME PARAMS AS full_collection 

    I WAS TOO LAZY TO UNPACK ALL THE NECESSARY ARGUEMENTS HERE, SO YOU'LL HAVE TO DO THEM MANUALLY 
    """

    ### defining test vars ### 
    test_tag = hashtags[0]
    dates_range = to_find[test_tag]
    test_date = create_period_list(dates_range)[0]
    pickler = pickle_support()

    file_loc = FILE_PATH + f"{test_tag}_{test_date}_prem_test"

    ### making the call ### 
    tweets = api.make_query(hashtag= test_tag,
                            query_requirements= query_reqs,
                            results_to_get= results_per_call,
                            dates= test_date)

    ### take a look at raw just in case ### 
    pickler.dump_data(file_loc, tweets)

    ### get the 
    tweet_results = [(t['created_at'],' '.join(t.text.split())) for t in tweets]
    excel_dump(tweet_results, test_tag, excel_file)
    print('TEST SUCCESSFUL')

def run_collection():
    """
    inputs in the necessary arguements from the reference_objects file
    THIS METHOD WILL CALL full_collection
    """
    
    yaml_file = importObjects.CRED_FILE
    yaml_key = importObjects.YAML_KEY
    to_find = importObjects.TO_FIND

    results_per_call = importObjects.RESULTS_PER_CALL_FULL
    query_reqs = importObjects.QUERY_REQUIRMENTS

    excel_file = importObjects.FULL_SEARCH_FILE

    api = twitter_api(yaml_file, yaml_key)
    # api.check_token()

    hashtags = list(to_find.keys())


    full_collection(hashtags= hashtags,
             to_find= to_find,
             api= api,
             results_per_call= results_per_call,
             query_reqs= query_reqs,
             excel_file= excel_file)

    print("FINISHED")
if __name__ == "__main__":
    ...


    # test_run()
    



"""
OLD CODE, probably not useful LMAO;


def access_twitter_ob(twitter_ob):
    to_export= []
    for obs in twitter_ob:
        created_date = obs['created_at']
        try:
            full_text = obs['quoted_status']['extended_tweet']['full_text']
        except KeyError:
            try: 
                full_text = obs['extended_tweet']['full_text']
            except KeyError:
                full_text = obs['text']
                print(obs)
                count +=1 
                # sleep(100)
        
        full_text = ' '.join(full_text.split())
        all_favourties = obs['favorite_count']
        all_retweets = obs['retweet_count']

    
        to_export.append([created_date, full_text, all_favourties, all_retweets]) 
    return to_export


def one_off():
    yaml_file = importObjects.CRED_FILE
    yaml_key = importObjects.YAML_KEY

    results_per_call = importObjects.RESULTS_PER_CALL_FULL
    query_reqs= importObjects.QUERY_REQUIRMENTS

    # output_file = importObjects.NEW_EXCEL

    api = twitter_api(yaml_file, yaml_key)

    tag = "#support_caa_and_nrc"
    dates_pair = ("2019-12-01", "2020-02-01")

    file_loc = FILE_PATH + f"{tag}_last_request_2"
    pickler = pickle_support()

    raw_tweets = api.make_query(
                            hashtag= tag,
                            query_requirements= query_reqs,
                            results_to_get= results_per_call,
                            dates= dates_pair )
    pickler.dump_data(file_loc, raw_tweets)
    print('pickled raw data')
    # revamped_tweets = access_twitter_ob(raw_tweets)
    # print('tweets revamped')

    # excel_dump(revamped_tweets, tag, output_file)
    # print('excel finished')
"""