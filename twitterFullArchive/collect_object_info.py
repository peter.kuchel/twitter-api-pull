import pickle
from reference_objects import importObjects
from excel_export import excel_dump
from periods import create_period_list
from time import sleep 


FOLDER_PATH = importObjects.RAW_DUMP_PATH


def open_pickle_file(path_to_file):
    with open(path_to_file, 'rb') as in_file:
        return pickle.load(in_file)

def full_text(obs):
    try:
        full_text = obs['quoted_status']['extended_tweet']['full_text']
    except KeyError:
        try: 
            full_text = obs['extended_tweet']['full_text']
        except KeyError:
            full_text = obs['text']
    
    return ' '.join(full_text.split())

def location_info(ob, user_ob):
    place = ob['place']
    if place != None:
        return place['country_code']

    try: 
        location = user_ob['derived']['locations']
        return location[0]['country_code']

    except KeyError:
        return None 
    

def media_info(user_ob):
    try:
        media_info = user_ob['entities']['media'][0]
    except KeyError:
        return None, None 
    
    return media_info['media_url'], media_info['type']


def collect_criteria(ob):
    """
    Most of the attributes in the twitter object can be indexed similar to a
    dictionary e.g. value = dict['key] (maybe they are dictionaries, idk i can't remember lmao)

    feel free to add more / remove headings on the template sheet, in this case it is helpful (haha)

    please note that you might have to define your own methods to get the info from the fields you want if
    it is not here. 

    the ones above could probably be done better, but if you want to code some, use them as examples 
    """
    user_ob = ob['user']
    
    created_date = ob['created_at']
    text = full_text(ob)
    handle = user_ob['screen_name']
    country = location_info(ob, user_ob)
    language = ob['lang']
    all_favourties = ob['favorite_count']
    all_retweets = ob['retweet_count']
    all_quotes = ob['quote_count']
    media, media_type = media_info(ob)
    
    # these align with the headers in the template sheet
    full = [
        created_date,
        text,
        handle,
        language,
        country,
        all_favourties,
        all_retweets,
        all_quotes,
        media,
        media_type
        
    ]

    return full

def ob_criteria(xlsx_file):
    """
    opens all of the twitter objects and indexes them based off of what you want
    it is really confusing going thru the objects, and somewhere below in the old code is prolly something 
    to help you search thru them 

    more detail about the objects themselves are in the collect_criteria method
    """
    to_find = importObjects.TO_FIND

    hashtags = list(to_find.keys())
    
    for h in hashtags:
        dates = create_period_list(to_find[h])

        to_export= []
        for d in dates:
            file_name = FOLDER_PATH + f"{h}_{d}" + "_tweet_object"
            twitter_objects = open_pickle_file(file_name)

            for obs in twitter_objects: 
                info = collect_criteria(obs)
                to_export.append(info)
        

        excel_dump(to_export, h, xlsx_file)


if __name__ == '__main__':

    #IMPORT THE THINGS YOU WANT TO A NEW EXCEL FILE (STILL NEEDS A TEMPLATE SHEET)

    xlsx_file = importObjects.GET_RESULTS_FILE 
    ob_criteria(xlsx_file)


    ##############################################################################################

    # BELOW IS ALL OLD CODE, FEEL FREE TO LOOK THRU IT IF THERE IS ANYTHING USEFUL #

    # the_files = ["#support_caa_and_nrc_last_request", "#support_caa_and_nrc_last_request_2"]
    # excel_out = "oneOffTweets.xlsx"
    # hashtag = "#support_caa_and_nrc"
    # object_collect(the_files, excel_out, hashtag)
    

    

    ### CODE THAT WAS CLEANED AND IS NOW DOWN HERE: IDK IF IT IS USEFUL AT ALL TBH LMAO ### 
    """
    def object_collect(files_list, excel_out, h):

        to_export = []
        count = 0
    
        for f in files_list:
            twitter_objects = open_pickle_file(f)
            for obs in twitter_objects:
                created_date = obs['created_at']
                try:
                    full_text = obs['quoted_status']['extended_tweet']['full_text']
                except KeyError:
                    try: 
                        full_text = obs['extended_tweet']['full_text']
                    except KeyError:
                        full_text = obs['text']
                        count +=1 
                        
                
                full_text = ' '.join(full_text.split())
                all_favourties = obs['favorite_count']
                all_retweets = obs['retweet_count']


                to_export.append([created_date, full_text, all_favourties, all_retweets]) 
        print(count)
        

        print(len(to_export))
        seen = set()
        to_dump = []
        for t in to_export:
            if t[1] not in seen:
                seen.add(t[1])
                to_dump.append(t)
        excel_dump(to_dump, h, excel_out)


    def collect_all():
        to_find = importObjects.TO_FIND

        hashtags = list(to_find.keys())
        count = 0
        for h in hashtags:
            dates = create_period_list(to_find[h])

            to_export= []
            for d in dates:
                file_name = FOLDER_PATH + f"{h}_{d}" + "_tweet_object"
                twitter_objects = open_pickle_file(file_name)

                
                for obs in twitter_objects:
                    created_date = obs['created_at']
                    try:
                        full_text = obs['quoted_status']['extended_tweet']['full_text']
                    except KeyError:
                        try: 
                            full_text = obs['extended_tweet']['full_text']
                        except KeyError:
                            full_text = obs['text']
                            print(obs)
                            count +=1 
                            # sleep(100)
                    
                    full_text = ' '.join(full_text.split())
                    all_favourties = obs['favorite_count']
                    all_retweets = obs['retweet_count']

                
                    to_export.append([created_date, full_text, all_favourties, all_retweets]) 

            excel_dump(to_export, h, excel_output)

            print(count)

    """
    # xlsx_file = "C:\\Users\\peter\\Desktop\\python\\Twitter\\excel_files\\fullTweets.xlsx"
    # file_to_test = FOLDER_PATH + "#abolishice_('2018-08-04', '2018-08-05')_tweet_object"
    # obs = open_pickle_file(file_to_test)
    # print(len(obs))
    # # test_ob = obs[9]

    # for o in obs:
    #     for k in o.keys():
    #         print(k, type(o[k]))
    #     print()
        
    
    # print(test_ob['quoted_status']['extended_tweet']['full_text'])

    # keys_to_search = ['retweet_count', 'favorite_count']
    # for o in obs:
    #     try:
    #         print(o['quoted_status']['extended_tweet']['full_text'])
    #     except KeyError:
    #         print(o)
        # for k in keys_to_search:
        #     print(f"{k} for {o[k]}")

