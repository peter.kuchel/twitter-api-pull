# Twitter API FullArchive

Code used to pull from the twitter API for a sociology masters thesis. Includes the tweet objects pulled as examples and scripts used to pull and handle the objects.

Please note that it is a bit messy and it doesn't always follow good conventions (naming, libraries, etc.). Honestly looking at it now, it could have been done much better (LMAO), but it got the job done haha. 

There is a good description of everything in the scripts themselves, so I won't go into detail about each script here. Start with looking in the reference_objects.py script, it has details on how you should approach everything 

Feel free to reach out if you have any questions, I am more than happy to help. 





